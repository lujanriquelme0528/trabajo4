<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tasa_Cambios;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void {
        $tasa_cambios = [
            [
                'factor_multiplicacion' => 0.85, // Tasa de cambio de dólar a euro
                'activo' => true,
                'fecha'=>'2023-11-20',
                'moneda_origen_id' => 1,
                'moneda_destino_id' => 2,
                'agencia_id' => 1,
            ],
            [
                'factor_multiplicacion' => 0.91, // Tasa de cambio de dólar a libra esterlina
                'activo' => true,
                'fecha'=>'2023-11-19',
                'moneda_origen_id' => 1,
                'moneda_destino_id' => 3,
                'agencia_id' => 1,
            ],
            [
                'factor_multiplicacion' => 0.79, // Tasa de cambio de dólar a yen japones
                'activo' => true,
                'fecha'=>'2023-11-19',
                'moneda_origen_id' => 1,
                'moneda_destino_id' => 4,
                'agencia_id' => 1,
            ],
            [
                'factor_multiplicacion' => 7431, // Tasa de cambio de dólar a Guarani
                'activo' => true,
                'fecha'=>'2023-11-19',
                'moneda_origen_id' => 1,
                'moneda_destino_id' => 5,
                'agencia_id' => 1,
            ],
            [
                'factor_multiplicacion' => 8105, // Tasa de cambio de euro a guarani
                'activo' => true,
                'fecha'=>'2023-11-1',
                'moneda_origen_id' => 2,
                'moneda_destino_id' => 5,
                'agencia_id' => 1,
            ],
        ];
    
        // Inserta las tasas de cambio en la base de datos
        foreach ($tasa_cambios as $tasa_cambios) {
            Tasa_Cambios::create($tasa_cambios);
    }
    }
}