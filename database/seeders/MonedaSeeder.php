<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Monedas;
class MonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
                
        $monedas = [
            [
                'nombre' => 'Dolar',
                'simbolo' => '$',
                'cntdecimales' => 2,
                'activo' => true,
            ],
            [
                'nombre' => 'Euro',
                'simbolo' => '€',
                'cntdecimales' => 2,
                'activo' => true,
            ],
            [
                'nombre' => 'Libra esterlina',
                'simbolo' => '£',
                'cntdecimales' => 2,
                'activo' => true,
            ],
            [
                'nombre' => 'Yen japonés',
                'simbolo' => '¥',
                'cntdecimales' => 0,
                'activo' => true,
            ],
            [
                'nombre' => 'Guarani',
                'simbolo' => 'G',
                'cntdecimales' => 0,
                'activo' => true,
        ],
];

    foreach ($monedas as $moneda) {
        Monedas::create($moneda);
    }
    }
}
