<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Agencias;

class AgenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $agencias = [
            [
                'nombre' => 'Lola',
                'sitio_web_url' => 'lola@gmail.com',
            ],
            [
                'nombre' => 'Lolu',
                'sitio_web_url' => 'lujan@gmail.com',
            ],
            [
                'nombre' => 'Alan',
                'sitio_web_url' => 'alan@gmail.com',
            ],
            [
                'nombre' => 'Rodri',
                'sitio_web_url' => 'ro@gmail.com',
            ], [
                'nombre' => 'adrian',
                'sitio_web_url' => 'adri@gmail.com',
            ],
        ];
    
        foreach ($agencias as $agencia) {
            Agencias::create($agencia);
        }
    }
}
