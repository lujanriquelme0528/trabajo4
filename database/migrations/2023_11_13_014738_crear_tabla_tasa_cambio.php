<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasa_cambio', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->decimal("factor_multiplicacion",8,2);
            $table->boolean('activo')->default(true);
            $table->date("fecha");
           
            $table->unsignedBigInteger('moneda_origen_id');
            $table->unsignedBigInteger('moneda_destino_id');
            $table->unsignedBigInteger('agencia_id');

           $table->foreign('moneda_origen_id')->references('id')->on('monedas');
            $table->foreign('moneda_destino_id')->references('id')->on('monedas');
            $table->foreign('agencia_id')->references('id')->on('agencia');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasa_cambio');
    }
};
