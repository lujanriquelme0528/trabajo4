<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
{
    /*$this->registerPolicies();

    Gate::define('social-login', function ($user) {
        return $user->provider && $user->provider != 'local';
    });*/

    }
}
