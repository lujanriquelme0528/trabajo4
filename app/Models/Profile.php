<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'avatar',
        'external_id',
        'external_auth',
        'user_id', // Asegúrate de que esta columna exista en tu tabla de perfiles
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
