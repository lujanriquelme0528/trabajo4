<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Agencias extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $table = 'agencia';
    protected $fillable = ['nombre', 'sitio_web_url'];


    public function tasa_cambios(): HasMany
    {
        return $this->hasMany(Tasa_Cambios::class, 'agencia_id')->where('activo',true);
    }

}