<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tasa_Cambios extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $table = 'tasa_cambio';
    protected $fillable = ['factor_multiplicacion','activo','fecha'];

    public function agencias(): BelongsTo
    {
        return $this->belongsTo(Agencias::class, 'agencia_id', 'id');
    }

    public function monedaOrigen(): BelongsTo
    {
        return $this->belongsTo(Monedas::class, 'moneda_origen_id', 'id');
    }

    public function monedaDestino(): BelongsTo
    {
        return $this->belongsTo(Monedas::class, 'moneda_destino_id','id');
}
}