<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Monedas extends Model
{
    use HasFactory;
    public $timestamps = true;

    protected $table = 'monedas';
    protected $fillable = ['nombre', 'simbolo', 'cntdecimales', 'activo'];

    public function tasa_cambios(): HasMany
    {
        return $this->hasMany(Tasa_Cambios::class, 'moneda_origen_id')->where('activo',true);
    }
}
