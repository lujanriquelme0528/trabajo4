<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'user.name' => 'required|string',
            'user.email' => 'required|string|email|unique:users',
            'profile.avatar' => 'nullable|string',
            'profile.external_id' => 'nullable|string',
            'profile.external_auth' => 'nullable|string',
        ]);

        return DB::transaction(function () use ($request) {
            $user = User::create($request->user);
            $profile = Profile::create([
                'avatar' => $request->profile['avatar'],
                'external_id' => $request->profile['external_id'],
                'external_auth' => $request->profile['external_auth'],
                'user_id' => $user->id, // Asegúrate de que la relación esté definida correctamente en el modelo
            ]);

            return response()->json([
                'user' => $user,
                'profile' => $profile,
            ],   201);
        });
    }
}
