<?php

namespace App\Http\Controllers;

use App\Models\Monedas;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MonedasController extends Controller
{
    public function index(Request $request)
    {
        $query = Monedas::query();

       // Búsqueda por nombre (insensible a mayúsculas/minúsculas)
       if ($request->filled('nombre')) {
        $query->whereRaw('LOWER(nombre) LIKE ?', ['%' . strtolower($request->input('nombre')) . '%']);
       }
        // Búsqueda por símbolo
        if ($request->filled('simbolo')) {
            $query->where('simbolo', $request->input('simbolo'));
        }
        
// Ordenar por id ascendente
$query->orderBy('id', 'asc');
//if $monedas -> ('activo',true); Aplicarle un if
/*$query->whereDoesntHave('tasa_cambios', function ($subquery) {
    $subquery->where('activo',true);
});*/
        $monedas = $query->get();

        return view('moneda', ['monedas' => $monedas]);
}
}