<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Log; // Importa la clase Log

class UserController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
{
    /*try {
        $userSocial = Socialite::driver($provider)->user();
    } catch (\Exception $e) {
        // Registra el error en los logs de Laravel
        Log::error('Error al obtener información del proveedor social: ' . $e->getMessage());
    // Redirige al usuario a la página de bienvenida
    //return redirect()->back()->with('error', 'Error al autenticar con el proveedor social. Por favor, inténtalo de nuevo más tarde.');
    //return redirect()->route('logueo', ['provider' => $provider]);
}
   /* $user = User::where(['email' => $userSocial->getEmail()])->first();

    if (!$user) {
        // Crear nuevo usuario si no existe
        $user = User::create([
            'name' => $userSocial->getName(),
            'email' => $userSocial->getEmail(),
            'provider' => $provider, // Agregar el proveedor social
            'provider_id' => $userSocial->getId(), // Agregar el ID del usuario en el proveedor social
        ]);
    }
    auth()->login($user);

    // Redirige al usuario a la página de bienvenida
    return redirect()->route('welcome');*/
}}
?>