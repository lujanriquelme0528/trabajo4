<?php

namespace App\Http\Controllers;

use App\Models\Agencias;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AgenciaController extends Controller
{
    public function index(Request $request)
    {
        $query = Agencias::query();

       // Búsqueda por nombre (insensible a mayúsculas/minúsculas)
       if ($request->filled('nombre')) {
        $query->whereRaw('LOWER(nombre) LIKE ?', ['%' . strtolower($request->input('nombre')) . '%']);
    }

    $agencias = $query->get();

    return view('agencia', ['agencias'=>$agencias]);;
} 
}
