<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agencias;
use App\Models\Monedas;
use App\Models\Tasa_Cambios;

class Tasa_CambiosController extends Controller
{
    public function indexMonedas(Request $request, $monedaId)
    {
        $moneda = Monedas::where('nombre', 'like', '%' . $request->get('nombre') . '%')
            ->orWhere('simbolo', $request->get('simbolo'))
            ->orderBy('activo', 'desc')
            ->get();

        return view('tasa_cambios.indexMonedas', [
            'monedas' => $moneda,
        ]);
    }

    public function indexAgencias(Request $request)
    {
        $agencias = Agencias::where('nombre', 'like', '%' . $request->get('nombre') . '%')
            ->orderBy('activo', 'desc')
            ->get();

        return view('tasa_cambios.indexAgencias', [
            'agencias' => $agencias,
        ]);
    }

    public function showTasaCambio($id)
    {
        $moneda = Monedas::findOrFail($id);
        $tasas = Tasa_Cambios::where('moneda_origen_id', $id)
            ->where('activo', true)

                    ->orderBy('fecha', 'desc')
            
            ->get();

        return view('tasa_cambios.index', compact('moneda', 'tasas'));
    }

    public function index($monedaId)
    {
        $moneda = Monedas::findOrFail($monedaId);

        $tasa_cambios = Tasa_Cambios::where('moneda_origen_id', $monedaId)
            ->orWhere('moneda_destino_id', $monedaId)
            ->where('activo', true)
                    ->orderBy('fecha', 'desc')
            ->get();

        return view('tasa_cambio', compact( 'moneda','tasa_cambios'));
}
}