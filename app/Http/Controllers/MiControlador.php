<?php

// app/Http/Controllers/MiControlador.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiControlador extends Controller
{
    public function mostrarNuevaVista()
    {
        $usuarioAutenticado = auth()->check();
        $nombreUsuario = $usuarioAutenticado ? auth()->user()->name : null;

        return view('nueva_vista', compact('usuarioAutenticado', 'nombreUsuario'));
    }
    public function store(Request $request)
    {
        // Validación de los datos de entrada
        $validatedData = $request->validate([
            'tabla1.email' => 'required|email',
            'tabla2.email' => 'required|email',
        ]);
    
        // Iniciar una transacción de base de datos
        DB::beginTransaction();
    
        try {
            // Crear el registro en la tabla1
            $tabla1 = Tabla1::create(['email' => $validatedData['tabla1']['email']]);
    
            // Crear el registro en la tabla2
            $tabla2 = Tabla2::create(['email' => $validatedData['tabla2']['email']]);
    
            // Confirmar la transacción
            DB::commit();
    
            // Preparar la respuesta
            $response = [
                'tabla1' => [
                    'id' => $tabla1->id,
                    'email' => $tabla1->email,
                ],
                'tabla2' => [
                    'id' => $tabla2->id,
                    'email' => $tabla2->email,
                ],
            ];
    
            // Devolver la respuesta con el código de estado   201
            return response()->json($response,   201);
        } catch (\Exception $e) {
            // Revertir la transacción en caso de error
            DB::rollBack();
    
            // Manejar el error
            return response()->json(['error' => 'Error al crear la transacción'],   500);
        }    
}

}
