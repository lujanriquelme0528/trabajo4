<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar sesión</title>
    <!-- Enlace al archivo CSS de Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Personalización de estilo para la vista de inicio -->
    <style>
        body {
            background-color: #f8f9fa; /* Color de fondo claro */
        }
        .container {
            max-width: 400px; /* Ancho máximo para centrar el contenido */
            margin: auto; /* Centrado horizontal */
            padding: 2rem; /* Espaciado interno */
        }
        .btn-login {
            display: block; /* Botón de inicio de sesión en su propia línea */
            width: 100%; /* Ancho completo del contenedor */
            margin-top: 1rem; /* Espacio arriba del botón */
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="text-center my-4">Bienvenido</h1>
        <p class="text-center">Inicia sesión con tu cuenta de Google.</p>
        <a href="{{ route('/login-google') }}" class="btn btn-lg btn-block btn-primary btn-login">Iniciar sesión con Google</a>
    </div>
    <!-- Scripts de Bootstrap -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
