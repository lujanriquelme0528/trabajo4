<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lorena - Agencias</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Agencias</h1>

    <!-- Formulario de búsqueda -->
    <form method="GET" action="{{ route('agencias') }}">
        <label for="nombre">Buscar por Nombre:</label>
        <input type="text" name="nombre" id="nombre" value="{{ request('nombre') }}">

        <button type="submit">Buscar</button>
    </form>

    <!-- Tabla de agencias -->
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Sitio Web</th>
                <th>Tasas Activas</th>
            </tr>
        </thead>
        <tbody>
            @foreach($agencias as $agencia)
                <tr>
                    <td>{{ $agencia->id }}</td>
                    <td>{{ $agencia->nombre }}</td>
                    <td>
                        <a href="{{ $agencia->sitio_web_url }}" target="_blank">{{ $agencia->sitio_web_url }}</a>
                    </td>
                    <td>{{ $agencia->tasa_cambios->count() }}</td>
                    <td>
                        <!-- Puedes agregar más acciones según sea necesario -->
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
<footer>
    <a href="{{ url('/') }}">Volver</a>
</footer>
</html>