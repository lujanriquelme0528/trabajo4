<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lorena - Monedas</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Incluir el archivo de estilos personalizado -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Monedas</h1>

    <!-- Formulario de búsqueda -->
    <form method="GET" action="{{ route('monedas') }}">
        <label for="nombre">Buscar por Nombre:</label>
        <input type="text" name="nombre" id="nombre" value="{{ request('nombre') }}">

        <label for="simbolo">Buscar por Símbolo:</label>
        <select name="simbolo" id="simbolo">
            <option value="">Seleccione...</option>
            <option value="$" @if(request('simbolo') == '$') selected @endif>$</option>
            <option value="€" @if(request('simbolo') == '€') selected @endif>€</option>
            <option value="£" @if(request('simbolo') == '£') selected @endif>£</option>
            <option value="¥" @if(request('simbolo') == '¥') selected @endif>¥</option>
            <option value="G" @if(request('simbolo') == 'G') selected @endif>G</option>
            <!-- Agrega opciones para otros símbolos -->
        </select>

        <button type="submit">Buscar</button>
    </form>

    <!-- Tabla de monedas -->
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Símbolo</th>
                <th>Decimales</th>
                <th>Activo</th>
                <th>Tasas Activas</th>
                <th>Acciones</th> <!-- Nueva columna para el enlace a la página 3 -->
            </tr>
        </thead>
        <tbody>
            @foreach($monedas as $moneda)
                <tr>
                    <td>{{ $moneda->id }}</td>
                    <td>{{ $moneda->nombre }}</td>
                    <td>{{ $moneda->simbolo }}</td>
                    <td>{{ $moneda->cntdecimales }}</td>
                    <td class="{{ $moneda->activo ? 'activo' : 'inactivo' }}">
                        {{ $moneda->activo ? 'Sí' : 'No' }}
                    </td>
                    <td>{{ $moneda->tasa_cambios->count() }}</td>
                    <td>
                        <a href="{{ route('tasa_cambios.index', ['id' => $moneda->id]) }}">Ver Tasas de Cambio</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
<footer>
    <a href="{{ url('/') }}">Volver</a>
</footer>
</html>