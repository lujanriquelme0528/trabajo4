<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Casa de Cambios</title>
    <!-- Enlace al archivo CSS de Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <h2>Alumna: Lorena Lujan Riquelme Riveros</h2>
    <h1>Casa de Cambios</h1>
    <img src="{{ asset('DER/t4-riquelme.jpg') }}" alt="DER" class="img-fluid">

    <!-- Enlaces a las páginas -->
    <nav>
        <ul>
            <li><a href="{{ route('monedas') }}">Monedas</a></li>
            <li><a href="{{ route('agencias') }}">Agencia</a></li>
        </ul>
    </nav>
</body>
</html>