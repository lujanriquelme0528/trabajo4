<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lorena - Tasa Cambios</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Tasa Cambios - {{ $moneda->nombre }}</h1>

    <!-- Información de la moneda -->
    <p>Nombre de la Moneda: {{ $moneda->nombre }}</p>
    <p>Símbolo de la Moneda: {{ $moneda->simbolo }}</p>
    <p>Cantidad de Decimales: {{ $moneda->cntdecimales }}</p>

    <!-- Grilla de tasas de cambio -->
    <div class="container">
        <h2>{{ $moneda->nombre }}</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>Moneda</th>
                    <th>Agencia</th>
                    <th>Fecha</th>
                    <th>Tasa de Cambio</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tasa_cambios as $tasa_cambio)
    <tr>
        <td>{{ optional($tasa_cambio->monedaOrigen)->nombre }}</td>
        <td>{{ optional($tasa_cambio->agencias)->nombre }}</td>
        <td>{{ $tasa_cambio->fecha }}</td>
        <td>{{ $tasa_cambio->factor_multiplicacion }}</td>
    </tr>
@endforeach

</tbody>
</table>
</div>

    <footer>
        <a href="{{ url('/moneda') }}">Volver</a>
    </footer>
</body>
</html>