<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MonedasController;
use App\Http\Controllers\AgenciaController;
use App\Http\Controllers\Tasa_CambiosController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MiControlador;
use App\Models\User;
// Rutas públicas
Route::get('/', function () {
    return view('logueo');
});
Route::get('/login-google', function () {
    return Socialite::driver('google')->redirect();
});
Route::get('/google-callback', function () {
    //$user = Socialite::driver('google')->user();
    return view('welcome', ['user' => Auth::user()]);

    $userExists = User::where('external_id',$user->id)->where('external_auth','google')->first();
    if($userExists){
        Auth::login($userExists);
    }else {
       $userNew = User::Create([
            'name'=> $user->name,
            'email'=> $user->email,
            'avatar'=> $user->avatar,
            'external_id'=> $user->id,
            'external_auth'=> 'google',
        ]);

        Auth::login($userNew);
    }
    return redirect('/');
    
});
// Rutas protegidas por autenticación
//Route::middleware(['auth'])->group(function () {
    Route::get('/welcome', [HomeController::class, 'index'])->name('welcome');

    Route::get('/moneda', [MonedasController::class, 'index'])->name('monedas');
    Route::get('/agencia', [AgenciaController::class, 'index'])->name('agencias');
    Route::get('/tasa_cambio/{moneda}', [Tasa_CambiosController::class, 'index'])->name('tasa_cambios.index');
    Route::get('/tasa_cambio/monedas', [Tasa_CambiosController::class, 'indexMonedas'])->name('tasa_cambios.indexMonedas');
    Route::get('/tasa_cambio/agencias', [Tasa_CambiosController::class, 'indexAgencias'])->name('tasa_cambios.indexAgencias');
    Route::get('/tasa_cambio/{id}', [Tasa_CambiosController::class, 'showTasaCambio'])->name('tasa_cambios.show');
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);

Route::get('/mostrar-nueva-vista', [MiControlador::class, 'mostrarNuevaVista'])->name('mostrar_nueva_vista');

//});

Route::get('/', function () {
    return view('welcome');
});
// Rutas de autenticación social

?>